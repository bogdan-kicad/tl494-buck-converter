EESchema Schematic File Version 4
LIBS:TL494_Buck-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L My_Library:TL494_Board U2
U 1 1 5D7E4EB8
P 3600 4100
F 0 "U2" H 3437 4625 50  0000 C CNN
F 1 "TL494_Board" H 3437 4534 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 3600 4250 50  0001 C CNN
F 3 "" H 3600 4250 50  0001 C CNN
	1    3600 4100
	1    0    0    -1  
$EndComp
$Comp
L Driver_FET:IR2110 U3
U 1 1 5D7E534C
P 4900 3900
F 0 "U3" H 5000 4600 50  0000 C CNN
F 1 "IR2110" H 5050 4500 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 4900 3900 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/ir2110.pdf?fileId=5546d462533600a4015355c80333167e" H 4900 3900 50  0001 C CNN
	1    4900 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3900 3600 3900
Wire Wire Line
	3600 4000 4600 4000
$Comp
L power:GND #PWR0101
U 1 1 5D7E6430
P 4800 4550
F 0 "#PWR0101" H 4800 4300 50  0001 C CNN
F 1 "GND" H 4805 4377 50  0000 C CNN
F 2 "" H 4800 4550 50  0001 C CNN
F 3 "" H 4800 4550 50  0001 C CNN
	1    4800 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4550 4800 4500
Wire Wire Line
	4900 4400 4900 4500
Wire Wire Line
	4900 4500 4800 4500
Connection ~ 4800 4500
Wire Wire Line
	4800 4500 4800 4400
Wire Wire Line
	4800 4500 4600 4500
Wire Wire Line
	4600 4500 4600 4100
$Comp
L Regulator_Linear:LM7812_TO220 U1
U 1 1 5D7E7284
P 3250 1750
F 0 "U1" H 3250 1992 50  0000 C CNN
F 1 "LM7812_TO220" H 3250 1901 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3250 1975 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 3250 1700 50  0001 C CNN
	1    3250 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D7E7F02
P 1600 1750
F 0 "J1" H 1708 1931 50  0000 C CNN
F 1 "IN" H 1708 1840 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 1600 1750 50  0001 C CNN
F 3 "~" H 1600 1750 50  0001 C CNN
	1    1600 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D7E8AB2
P 1900 1950
F 0 "#PWR0102" H 1900 1700 50  0001 C CNN
F 1 "GND" H 1905 1777 50  0000 C CNN
F 2 "" H 1900 1950 50  0001 C CNN
F 3 "" H 1900 1950 50  0001 C CNN
	1    1900 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 1950 1900 1850
Wire Wire Line
	1900 1850 1800 1850
$Comp
L Device:C_Small C2
U 1 1 5D7E922D
P 2700 1950
F 0 "C2" H 2792 1996 50  0000 L CNN
F 1 "0.1u" H 2792 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2700 1950 50  0001 C CNN
F 3 "~" H 2700 1950 50  0001 C CNN
	1    2700 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1750 2250 1750
$Comp
L power:GND #PWR0103
U 1 1 5D7EBB27
P 2700 2050
F 0 "#PWR0103" H 2700 1800 50  0001 C CNN
F 1 "GND" H 2705 1877 50  0000 C CNN
F 2 "" H 2700 2050 50  0001 C CNN
F 3 "" H 2700 2050 50  0001 C CNN
	1    2700 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D7EBDDC
P 3250 2050
F 0 "#PWR0104" H 3250 1800 50  0001 C CNN
F 1 "GND" H 3255 1877 50  0000 C CNN
F 2 "" H 3250 2050 50  0001 C CNN
F 3 "" H 3250 2050 50  0001 C CNN
	1    3250 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1850 2700 1750
Connection ~ 2700 1750
Wire Wire Line
	2700 1750 2950 1750
$Comp
L Device:CP_Small C1
U 1 1 5D7EC557
P 2250 1950
F 0 "C1" H 2338 1996 50  0000 L CNN
F 1 "1000u" H 2338 1905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 2250 1950 50  0001 C CNN
F 3 "~" H 2250 1950 50  0001 C CNN
	1    2250 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1850 2250 1750
Connection ~ 2250 1750
Wire Wire Line
	2250 1750 2700 1750
$Comp
L power:GND #PWR0105
U 1 1 5D7EDB4B
P 2250 2050
F 0 "#PWR0105" H 2250 1800 50  0001 C CNN
F 1 "GND" H 2255 1877 50  0000 C CNN
F 2 "" H 2250 2050 50  0001 C CNN
F 3 "" H 2250 2050 50  0001 C CNN
	1    2250 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C3
U 1 1 5D7EE6AE
P 3700 1950
F 0 "C3" H 3788 1996 50  0000 L CNN
F 1 "1u" H 3788 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3700 1950 50  0001 C CNN
F 3 "~" H 3700 1950 50  0001 C CNN
	1    3700 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5D7EECA1
P 3700 2050
F 0 "#PWR0106" H 3700 1800 50  0001 C CNN
F 1 "GND" H 3705 1877 50  0000 C CNN
F 2 "" H 3700 2050 50  0001 C CNN
F 3 "" H 3700 2050 50  0001 C CNN
	1    3700 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1850 3700 1750
Wire Wire Line
	3700 1750 3550 1750
Wire Wire Line
	3700 1750 4050 1750
Wire Wire Line
	4900 1750 4900 2950
Connection ~ 3700 1750
$Comp
L Device:C_Small C4
U 1 1 5D7F36B8
P 4050 1950
F 0 "C4" H 4142 1996 50  0000 L CNN
F 1 "0.1u" H 4142 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 4050 1950 50  0001 C CNN
F 3 "~" H 4050 1950 50  0001 C CNN
	1    4050 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5D7F3AEE
P 4050 2050
F 0 "#PWR0107" H 4050 1800 50  0001 C CNN
F 1 "GND" H 4055 1877 50  0000 C CNN
F 2 "" H 4050 2050 50  0001 C CNN
F 3 "" H 4050 2050 50  0001 C CNN
	1    4050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1850 4050 1750
Connection ~ 4050 1750
Wire Wire Line
	4050 1750 4900 1750
Wire Wire Line
	3600 3800 4250 3800
Wire Wire Line
	4250 3800 4250 2950
Wire Wire Line
	4250 2950 4900 2950
Connection ~ 4900 2950
Wire Wire Line
	4900 2950 4900 3400
$Comp
L Device:C_Small C5
U 1 1 5D7F668F
P 5350 3850
F 0 "C5" H 5442 3896 50  0000 L CNN
F 1 "100n" H 5442 3805 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5350 3850 50  0001 C CNN
F 3 "~" H 5350 3850 50  0001 C CNN
	1    5350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4000 5350 4000
Wire Wire Line
	5350 4000 5350 3950
Wire Wire Line
	5350 3750 5350 3600
Wire Wire Line
	5350 3600 5200 3600
$Comp
L Device:D D1
U 1 1 5D7F787D
P 5350 3250
F 0 "D1" V 5396 3171 50  0000 R CNN
F 1 "1N4148" V 5305 3171 50  0000 R CNN
F 2 "Diode_THT:D_T-1_P5.08mm_Horizontal" H 5350 3250 50  0001 C CNN
F 3 "~" H 5350 3250 50  0001 C CNN
	1    5350 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 4100 5850 2950
Wire Wire Line
	5200 4100 5850 4100
Wire Wire Line
	4900 2950 5350 2950
Wire Wire Line
	5350 3600 5350 3400
Connection ~ 5350 3600
Wire Wire Line
	5350 3100 5350 2950
Connection ~ 5350 2950
Wire Wire Line
	5350 2950 5850 2950
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 5D7F9C3B
P 6450 3400
F 0 "Q1" H 6656 3446 50  0000 L CNN
F 1 "IRF510" H 6656 3355 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6650 3500 50  0001 C CNN
F 3 "~" H 6450 3400 50  0001 C CNN
	1    6450 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q2
U 1 1 5D7FA8A3
P 6450 4200
F 0 "Q2" H 6656 4246 50  0000 L CNN
F 1 "IRF510" H 6656 4155 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6650 4300 50  0001 C CNN
F 3 "~" H 6450 4200 50  0001 C CNN
	1    6450 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 4200 5200 4200
Wire Wire Line
	5350 4000 6100 4000
Wire Wire Line
	6100 4000 6100 3800
Wire Wire Line
	6100 3800 6550 3800
Wire Wire Line
	6550 3800 6550 3600
Connection ~ 5350 4000
Wire Wire Line
	6550 3800 6550 4000
Connection ~ 6550 3800
Wire Wire Line
	5200 3700 6100 3700
Wire Wire Line
	6100 3700 6100 3400
Wire Wire Line
	6100 3400 6250 3400
Wire Wire Line
	6550 3200 6550 1350
Wire Wire Line
	6550 1350 2250 1350
Wire Wire Line
	2250 1350 2250 1750
$Comp
L pspice:INDUCTOR L1
U 1 1 5D800EE2
P 7100 3800
F 0 "L1" H 7100 4015 50  0000 C CNN
F 1 "INDUCTOR" H 7100 3924 50  0000 C CNN
F 2 "Inductor_THT:L_Toroid_Vertical_L24.6mm_W15.5mm_P11.44mm_Pulse_KM-4" H 7100 3800 50  0001 C CNN
F 3 "~" H 7100 3800 50  0001 C CNN
	1    7100 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 3800 6550 3800
$Comp
L Device:CP C6
U 1 1 5D8060F5
P 7650 4100
F 0 "C6" H 7768 4146 50  0000 L CNN
F 1 "1000u" H 7768 4055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 7688 3950 50  0001 C CNN
F 3 "~" H 7650 4100 50  0001 C CNN
	1    7650 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3950 7650 3800
Wire Wire Line
	7650 3800 7350 3800
$Comp
L power:GND #PWR0108
U 1 1 5D80732D
P 7650 4250
F 0 "#PWR0108" H 7650 4000 50  0001 C CNN
F 1 "GND" H 7655 4077 50  0000 C CNN
F 2 "" H 7650 4250 50  0001 C CNN
F 3 "" H 7650 4250 50  0001 C CNN
	1    7650 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C7
U 1 1 5D807A4F
P 8150 4100
F 0 "C7" H 8268 4146 50  0000 L CNN
F 1 "1000u" H 8268 4055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 8188 3950 50  0001 C CNN
F 3 "~" H 8150 4100 50  0001 C CNN
	1    8150 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5D807E28
P 8150 4250
F 0 "#PWR0109" H 8150 4000 50  0001 C CNN
F 1 "GND" H 8155 4077 50  0000 C CNN
F 2 "" H 8150 4250 50  0001 C CNN
F 3 "" H 8150 4250 50  0001 C CNN
	1    8150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 3950 8150 3800
Wire Wire Line
	8150 3800 7650 3800
Connection ~ 7650 3800
$Comp
L power:GND #PWR0110
U 1 1 5D7FCEB5
P 6550 4400
F 0 "#PWR0110" H 6550 4150 50  0001 C CNN
F 1 "GND" H 6555 4227 50  0000 C CNN
F 2 "" H 6550 4400 50  0001 C CNN
F 3 "" H 6550 4400 50  0001 C CNN
	1    6550 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D80C9DB
P 9450 4200
F 0 "R2" H 9520 4246 50  0000 L CNN
F 1 "I_SENSE" H 9520 4155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P5.08mm_Vertical" V 9380 4200 50  0001 C CNN
F 3 "~" H 9450 4200 50  0001 C CNN
	1    9450 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5D80CF16
P 9450 4350
F 0 "#PWR0111" H 9450 4100 50  0001 C CNN
F 1 "GND" H 9455 4177 50  0000 C CNN
F 2 "" H 9450 4350 50  0001 C CNN
F 3 "" H 9450 4350 50  0001 C CNN
	1    9450 4350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D80D6FF
P 9800 3900
F 0 "J2" H 9772 3782 50  0000 R CNN
F 1 "OUT" H 9772 3873 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 9800 3900 50  0001 C CNN
F 3 "~" H 9800 3900 50  0001 C CNN
	1    9800 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	9450 4050 9450 3900
Wire Wire Line
	9450 3900 9600 3900
Wire Wire Line
	9450 3900 9300 3900
Wire Wire Line
	9300 3900 9300 5500
Wire Wire Line
	3900 5500 3900 4300
Wire Wire Line
	3900 4300 3600 4300
Connection ~ 9450 3900
$Comp
L power:GND #PWR0112
U 1 1 5D813F18
P 3600 4400
F 0 "#PWR0112" H 3600 4150 50  0001 C CNN
F 1 "GND" H 3605 4227 50  0000 C CNN
F 2 "" H 3600 4400 50  0001 C CNN
F 3 "" H 3600 4400 50  0001 C CNN
	1    3600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 3800 9000 3800
Connection ~ 9000 3800
$Comp
L Device:R_POT RV1
U 1 1 5D81600F
P 9000 4950
F 0 "RV1" H 8930 4904 50  0000 R CNN
F 1 "10k" H 8930 4995 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 9000 4950 50  0001 C CNN
F 3 "~" H 9000 4950 50  0001 C CNN
	1    9000 4950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5D8164E6
P 9000 5100
F 0 "#PWR0113" H 9000 4850 50  0001 C CNN
F 1 "GND" H 9005 4927 50  0000 C CNN
F 2 "" H 9000 5100 50  0001 C CNN
F 3 "" H 9000 5100 50  0001 C CNN
	1    9000 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 3800 9000 4800
Wire Wire Line
	4250 4950 4250 4200
Wire Wire Line
	4250 4200 3600 4200
$Comp
L Device:R R1
U 1 1 5D825FFC
P 8650 4100
F 0 "R1" H 8720 4146 50  0000 L CNN
F 1 "R_DIS" H 8720 4055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P5.08mm_Vertical" V 8580 4100 50  0001 C CNN
F 3 "~" H 8650 4100 50  0001 C CNN
	1    8650 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5D826649
P 8650 4250
F 0 "#PWR0114" H 8650 4000 50  0001 C CNN
F 1 "GND" H 8655 4077 50  0000 C CNN
F 2 "" H 8650 4250 50  0001 C CNN
F 3 "" H 8650 4250 50  0001 C CNN
	1    8650 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3950 8650 3800
Connection ~ 8650 3800
Wire Wire Line
	8650 3800 9000 3800
Wire Wire Line
	8150 3800 8650 3800
Connection ~ 8150 3800
Wire Wire Line
	4250 4950 8850 4950
Wire Wire Line
	3900 5500 9300 5500
$EndSCHEMATC
